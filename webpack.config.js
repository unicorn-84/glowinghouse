const path = require('path');
const glob = require('glob-all');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const PurgecssPlugin = require('purgecss-webpack-plugin');
const RobotstxtPlugin = require('robotstxt-webpack-plugin');
const AddAssetPlugin = require('add-asset-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

let prod;
if (process.env.npm_lifecycle_event === 'prod') {
  prod = true;
}

module.exports = {
  entry: {
    main: path.resolve(__dirname, 'src', 'main.js'),
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: prod ? 'scripts/[name].[contenthash:4].js' : 'scripts/[name].js',
    chunkFilename: prod ? 'scripts/[name].[contenthash:4].js' : 'scripts/[name].js',
  },
  module: {
    rules: [
      // JS
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: [
          'babel-loader',
        ],
      },
      // SCSS
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: prod ? {
              publicPath: '../',
            } : {},
          },
          {
            loader: 'css-loader',
          },
          {
            loader: 'postcss-loader',
            options: {
              config: {
                ctx: {
                  cssnano: prod ? {
                    preset: ['default', {
                      discardComments: {
                        removeAll: true,
                      },
                    }],
                  } : false,
                },
              },
            },
          },
          'sass-loader',
        ],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: prod ? 'styles/main.[contenthash:4].css' : 'styles/main.css',
    }),
    new HtmlWebpackPlugin({
      template: 'src/templates/index.html',
      inject: true,
      minify: {
        removeComments: prod,
        minifyCSS: prod,
        minifyJS: prod,
        collapseWhitespace: prod,
      },
    }),
    new CopyPlugin([
      'src/configs/browserconfig.xml',
      'src/configs/site.webmanifest',
      'src/configs/.htaccess',
      {
        from: 'dist',
      },
    ]),
  ],
  devtool: 'cheap-module-source-map',
  devServer: {
    stats: 'errors-only',
    overlay: true,
    compress: true,
    port: 9001,
  },
  optimization: {
    noEmitOnErrors: true,
  },
};

(function purgeCss() {
  module.exports.plugins.push(
    new PurgecssPlugin({
      paths: glob.sync(path.resolve(__dirname, 'src/templates/*.html'), { nodir: true }),
    }),
  );
}());

(function makeSeoStuff() {
  module.exports.plugins.push(
    new RobotstxtPlugin({
      policy: [
        {
          userAgent: '*',
          disallow: prod ? null : '/',
        },
      ],
    }),
    new AddAssetPlugin('humans.txt',
      `/* TEAM */\nDeveloper: unicorn-84\nEmail: unicorn-84[at]ya[dot]ru\nLocation: St. Petersburg, Russia\n\n/* SITE */\nLast update: ${new Date().toLocaleDateString(
        'RU-ru', {
          year: 'numeric',
          month: '2-digit',
          day: '2-digit',
        },
      )}\nLanguage: Russian\nStandards: HTML5, CSS3, ES6\nIDE: WebStorm`),
  );
}());
